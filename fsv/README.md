# fsv

![Version: 0.1.5](https://img.shields.io/badge/Version-0.1.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for the FSV application stack to be run on Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/fsv-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| adminDomain | string | `""` | The domain used by admins and content creators to navigate to the Wagtail CMS and Django Admin of the application. |
| adminReplicaCount | int | `1` | Amount of replicas used for the FSV admin deployment. |
| adminResources | object | `{}` | Resource requests for the FSV main admin container. Left emptry intentionally. Set according to the target environment. |
| adminWhitelist | string | `""` | The allow list to control who can access the Django Admin and Wagtail CMS pages. |
| encryptedFilesSharedStorageSize | string | `""` | The size of the shared storage designated for encrypted files. |
| encryptedFilesStorage | string | `""` | The file system path to store encrypted files. |
| env | string | `""` | Setting the ENV variable equal to "test" will run the application in DEBUG mode. Only use for non-production environments. |
| externalApiHost | string | `""` | The host name of an external API to make retrieve data for a user by providing their BSN number. |
| externalLoadbalancerDomain | string | `""` | The domain used by external load balancers to send traffic to. This is primarily in place to facilitate DigiD authentication. |
| externalLoadbalancerWhitelist | string | `""` | The allow list for the domain used by external load balancers. This should only allow the specific load balancers configured to forward traffic to our servers. |
| features | list | `[]` | A range of features, used to toggle specific functionality on or off. |
| fromEmail | string | `""` | Default email address to use for various automated correspondence from the site manager(s). |
| imageRepository | string | `"registry.gitlab.com/pleio/dossier"` | Default image repository for the FSV application. |
| imageTag | string | `"1.0.0"` | Default FSV application image tag to use. |
| maintenance | string | `"False"` | A toggle used to display a maintenance page for each request. Set to "True" to enable, otherwise "False". |
| mediaSharedStorageSize | string | `""` | The size of the shared storage designated for media files, i.e. images and documents. |
| mediaStorage | string | `""` | The file system path to store media related to the application. |
| mockBsnChecker | bool | `false` | A toggle to turn the mock API on or off. Only use in non-production environments. |
| mockSamlConnection | bool | `false` | A toggle to turn the SimpleSAML IDP on or off. Only use in non-production environments. |
| monitoringDomain | string | `""` | The domain exposed to allow for the collection of logs and metrics. |
| monitoringWhitelist | string | `""` | The whitelist for the monitoring scraping endpoint. This should only allow the requests designated to collect logs and metrics. |
| nginxRepository | string | `"nginx"` | The image repository for the NGINX webserver. |
| nginxTag | string | `"1.21-alpine"` | The image tag for the NGINX webserver container. |
| samlEntityId | string | `""` | The SAML entity ID url where the SP published public information about itself. |
| samlMetadataUrl | string | `""` | The metadata url of the SAML Identity Provider used to exchange information regarding encryption and endpoints. |
| samlServiceId | string | `""` | The id of the service that can be consumed by login to this client. |
| sharedStorageClassName | string | `""` | The name of the Storage Class to be used for shared pod storage. Must be of access mode RWX. |
| site | string | `"fsv"` | The id of the FSV instance |
| siteName | string | `"FSV Portaal"` | The name of the FSV instance |
| siteNameLong | string | `"de FSV"` | The (long) name of the FSV instance |
| trustedCertsLocation | string | `""` | The list of CA certificates used by the python 'requests' module. |
| useLetsEncryptCertificate | bool | `false` | Whether a LetsEncrypt certificate should be issued by the cluster CertManager issues or not. |
| webDomain | string | `""` | The main domain used by users to navigate to the internet-facing side of the application. |
| webReplicaCount | int | `1` | Amount of replicas used for the FSV web deployment. |
| webResources | object | `{}` | Resource requests for the FSV main web container. Left emptry intentionally. Set according to the target environment. |
| webWhitelist | string | `""` | The allow list for the internet-facing portion of the application. Should not be used in production. |
| wildcardTlsSecretName | string | `""` | The name of the K8S secret containing the key pair for the wildcard certificate used. To obtain a LetsEncrypt certificate, set to a name not associated to any secret to allow cert-manager to request a certificate. |
