{{/*
The name of the release, used as label or a base for other names.
*/}}
{{- define "fsv.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{/*
The name used to labels resources associated with the web deployment.
*/}}
{{- define "fsv.webName" -}}
{{- printf "%s-web" (include "fsv.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the admin deployment.
*/}}
{{- define "fsv.adminName" -}}
{{- printf "%s-admin" (include "fsv.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the monitoring ingress.
*/}}
{{- define "fsv.monitoringName" -}}
{{- printf "%s-monitoring" (include "fsv.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock IDP deployment.
*/}}
{{- define "fsv.mockIdpName" -}}
{{- printf "%s-mock-idp" (include "fsv.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the NGINX webserver deployment.
*/}}
{{- define "fsv.webserverName" -}}
{{- printf "%s-nginx" (include "fsv.name" . ) -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock NGINX webserver deployment.
*/}}
{{- define "fsv.mockWebserverName" -}}
{{- printf "%s-mock-nginx" (include "fsv.name" . ) -}}
{{- end }}

{{/*
The name of the secret containing the keypair used to encrypt SAML traffic.
Through our metadata endpoint, this certificate is provided to external parties.
*/}}
{{- define "fsv.keypairSecretName" -}}
{{- printf "%s-keypair" (include "fsv.name" . ) -}}
{{- end }}

{{/*
The root CA to trust for the external API endpoint related to the BSN checker and other APIs.
*/}}
{{- define "fsv.rootCaSecretName" -}}
{{- printf "%s-root-ca" (include "fsv.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing media files, i.e. images and documents.
*/}}
{{- define "fsv.mediaSharedStoragePvcName" -}}
{{- printf "%s-media" (include "fsv.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing encrypted files.
*/}}
{{- define "fsv.encryptedFilesSharedStoragePvcName" -}}
{{- printf "%s-encrypted-files" (include "fsv.name" . ) -}}
{{- end }}

{{/*
A set of standard labels as defined by the Helm developers to use in a chart.
*/}}
{{- define "fsv.standardLabels" -}}
app.kubernetes.io/name: {{ include "fsv.name" . }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
